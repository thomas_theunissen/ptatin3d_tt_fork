/*@ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**
**    Copyright (c) 2012
**        Dave A. May [dave.may@erdw.ethz.ch]
**        Institute of Geophysics
**        ETH Zürich
**        Sonneggstrasse 5
**        CH-8092 Zürich
**        Switzerland
**
**    project:    pTatin3d
**    filename:   model_ops_template.c
**
**
**    pTatin3d is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published
**    by the Free Software Foundation, either version 3 of the License,
**    or (at your option) any later version.
**
**    pTatin3d is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
**    See the GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with pTatin3d. If not, see <http://www.gnu.org/licenses/>.
**
** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ @*/

#include "petsc.h"
#include "ptatin3d.h"
#include "private/ptatin_impl.h"
#include "ptatin3d_stokes.h"
#include "ptatin_models.h"
#include "model_template_ctx.h"

/*
  Purpose:
  - Parse command line options to initialize parameters in your model context (ModelTemplateCtx).
  - Configure flow law.
  - Set flow law parameters.
*/
static PetscErrorCode ModelInitialize(pTatinCtx c,void *ctx)
{
  ModelTemplateCtx *data;
  PetscBool        flg;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  
  data = (ModelTemplateCtx*)ctx;
  
  /* Parse command line options for parameters in model context */
  ierr = PetscOptionsGetReal(NULL,NULL,"-model_template_param1",&data->param1,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL, "-model_template_param2",&data->param2,&flg);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Define the physical dimensions of the model domain.
  - Specify any mesh refinement.
*/
static PetscErrorCode ModelApplyInitialMeshGeometry(pTatinCtx c,void *ctx)
{
  /* ModelTemplateCtx *data; */
  PhysCompStokes   stokes;
  DM               dav,dap;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);

  /* data = (ModelTemplateCtx*)ctx; */
  ierr = pTatinGetStokesContext(c,&stokes);CHKERRQ(ierr);
  ierr = PhysCompStokesGetDMs(stokes,&dav,&dap);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dav,0.0,1.0,0.0,1.0,0.0,0.1);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Assign regions to material points.
*/
static PetscErrorCode ModelApplyInitialMaterialGeometry(pTatinCtx c,void *ctx)
{
  /*ModelTemplateCtx *data;*/
  DataBucket       material_points;
  int              p,npoints;
  DataField        datafield_material_point;
  int              region_p;
  PetscErrorCode   ierr;
  
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  
  /* data = (ModelTemplateCtx*)ctx; */

  ierr = pTatinGetMaterialPoints(c,&material_points,NULL);CHKERRQ(ierr);
  DataBucketGetSizes(material_points,&npoints,NULL,NULL);
  
  DataBucketGetDataFieldByName(material_points,MPntStd_classname,&datafield_material_point);
  DataFieldGetAccess(datafield_material_point);
  for (p=0; p<npoints; p++) {
    MPntStd *mpoint;
    double  *coor_p;
    
    DataFieldAccessPoint(datafield_material_point,p,(void**)&mpoint);

    /* Get the coordinate of material point p */
    MPntStdGetField_global_coord(mpoint,&coor_p);
    
    region_p = -1; /* initial value for region */
    
    /* Perform some geometric query to determine the region index of material point p */
    if ((coor_p[0] > -1.0e32) && (coor_p[0] < 1.0e32)) {
      region_p = 0;
    }
    
    /* Set region index of material point p */
    MPntStdSetField_phase_index(mpoint,region_p);

  }
  DataFieldRestoreAccess(datafield_material_point);
  
  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Set the initial value for viscosity and density (and potentially others) on
    all material points.
 
  Optional method.
*/
static PetscErrorCode ModelApplyInitialStokesVariableMarkers(pTatinCtx c,Vec X,void *ctx)
{
  /*ModelTemplateCtx *data;*/
  DataBucket       material_points;
  int              p,npoints;
  DataField        datafield_material_point,datafield_material_point_stokes;
  double           eta_p,rho_p;
  PetscErrorCode   ierr;
  
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  
  /* data = (ModelTemplateCtx*)ctx; */
  
  ierr = pTatinGetMaterialPoints(c,&material_points,NULL);CHKERRQ(ierr);
  DataBucketGetSizes(material_points,&npoints,NULL,NULL);
  
  DataBucketGetDataFieldByName(material_points,MPntStd_classname,&datafield_material_point);
  DataFieldGetAccess(datafield_material_point);
  DataBucketGetDataFieldByName(material_points,MPntPStokes_classname,&datafield_material_point_stokes);
  DataFieldGetAccess(datafield_material_point_stokes);
  for (p=0; p<npoints; p++) {
    MPntStd     *mpoint;
    MPntPStokes *mpoint_stokes;
    double      *coor_p;
    int         region_p;
    
    DataFieldAccessPoint(datafield_material_point,p,(void**)&mpoint);
    DataFieldAccessPoint(datafield_material_point_stokes,p,(void**)&mpoint_stokes);
    
    /* Get the coordinate of material point p */
    MPntStdGetField_global_coord(mpoint,&coor_p);

    /* Get the region index of material point p */
    MPntStdGetField_phase_index(mpoint,&region_p);

    /* Set initial value */
    eta_p = 0.0;
    rho_p = 0.0;
    
    /* Possibly query region index and set eta, rho based on that */
    if (region_p >= 0) {
      eta_p = 1.1e1;
      rho_p = 0.33;
    }
    
    /* Set viscoity and density on material point p */
    MPntPStokesSetField_eta_effective(mpoint_stokes,eta_p);
    MPntPStokesSetField_density(mpoint_stokes,rho_p);
  }
  DataFieldRestoreAccess(datafield_material_point);
  DataFieldRestoreAccess(datafield_material_point_stokes);
  
  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Define the intitial condition for velocity and pressure (X = (u,p)).
  - If solving the energy equation, define initial condition for temperature.
 
  Optional method if only solving Stokes.
*/
static PetscErrorCode ModelApplyInitialSolution(pTatinCtx c,Vec X,void *ctx)
{
  /* ModelTemplateCtx *data; */

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  
  /* data = (ModelTemplateCtx*)ctx; */

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Specify boundary conditions for velocity.
*/
static PetscErrorCode ModelTemplate_VelocityBC(BCList bclist,DM dav,pTatinCtx c,ModelTemplateCtx *data)
{
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Impose boundary conditions on velocity using the user function ModelTemplate_VelocityBC().
  - If solving the energy equation, impose boundary conditions on temperature.
*/
static PetscErrorCode ModelApplyBoundaryCondition(pTatinCtx c,void *ctx)
{
  ModelTemplateCtx *data;
  PhysCompStokes   stokes;
  DM               dav,dap;
  BCList           bclist_v;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);

  data = (ModelTemplateCtx*)ctx;
  PetscPrintf(PETSC_COMM_WORLD,"param1 = %lf \n", data->param1 );

  /* Define velocity boundary conditions */
  ierr = pTatinGetStokesContext(c,&stokes);CHKERRQ(ierr);
  ierr = PhysCompStokesGetDMs(stokes,&dav,&dap);CHKERRQ(ierr);

  ierr = PhysCompStokesGetBCList(stokes,&bclist_v,NULL);CHKERRQ(ierr);
  ierr = ModelTemplate_VelocityBC(bclist_v,dav,c,data);CHKERRQ(ierr);

  /* Define boundary conditions for any other physics */

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Impose boundary conditions on velocity using the user function ModelTemplate_VelocityBC()
    on all multi-grid levels.
*/
static PetscErrorCode ModelApplyBoundaryConditionMG(PetscInt nl,BCList bclist[],DM dav[],pTatinCtx c,void *ctx)
{
  ModelTemplateCtx *data;
  PetscInt         n;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);

  data = (ModelTemplateCtx*)ctx;
  /* Define velocity boundary conditions on each level within the MG hierarchy */
  for (n=0; n<nl; n++) {
    ierr = ModelTemplate_VelocityBC(bclist[n],dav[n],c,data);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Define new material points within the domain. This may be required for example
    at places where there is inflow.
 
  Optional method.
*/
static PetscErrorCode ModelApplyMaterialBoundaryCondition(pTatinCtx c,void *ctx)
{
  /* ModelTemplateCtx *data; */

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  
  /* data = (ModelTemplateCtx*)ctx; */

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Define how the model domain (mesh) will deform.
 
  Optional method.
*/
static PetscErrorCode ModelApplyUpdateMeshGeometry(pTatinCtx c,Vec X,void *ctx)
{
  /* ModelTemplateCtx *data; */

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  
  /* data = (ModelTemplateCtx*)ctx; */

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Specify all required output types / styles you desire.
 
  The path where output will be written can be specified via the command line option
  -output_path
 
  Optional method.
*/
static PetscErrorCode ModelOutput(pTatinCtx c,Vec X,const char prefix[],void *ctx)
{
  /* ModelTemplateCtx *data; */
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n",PETSC_FUNCTION_NAME);
  
  /* data = (ModelTemplateCtx*)ctx; */

  /* ---- Velocity-Pressure Mesh Output ---- */
  /* [1] Standard viewer: v,p written out as binary in double */
  ierr = pTatin3d_ModelOutput_VelocityPressure_Stokes(c,X,prefix);CHKERRQ(ierr);
  
  /* [2] Light weight viewer: Only v is written out. v and coords are expressed as floats */
  /*
  ierr = pTatin3d_ModelOutputLite_Velocity_Stokes(c,X,prefix);CHKERRQ(ierr);
  */
  /* [3] Write out v,p into PETSc Vec. These can be used to restart pTatin */
  /*
  ierr = pTatin3d_ModelOutputPetscVec_VelocityPressure_Stokes(c,X,prefix);CHKERRQ(ierr);
  */

  /* ---- Material Point Output ---- */
  /* [1] Basic viewer: Only reports coords, regionid and other internal data */
  /*
  ierr = pTatin3d_ModelOutput_MPntStd(c,prefix);CHKERRQ(ierr);
  */

  /* [2] Customized viewer: User defines specific fields they want to view - NOTE not .pvd file will be created */
  /*
  {
  DataBucket                materialpoint_db;
  const int                 nf = 4;
  const MaterialPointField  mp_prop_list[] = { MPField_Std, MPField_Stokes, MPField_StokesPl, MPField_Energy };
  char                      mp_file_prefix[256];

  ierr = pTatinGetMaterialPoints(c,&materialpoint_db,NULL);CHKERRQ(ierr);
  sprintf(mp_file_prefix,"%s_mpoints",prefix);
  ierr = SwarmViewGeneric_ParaView(materialpoint_db,nf,mp_prop_list,c->outputpath,mp_file_prefix);CHKERRQ(ierr);
  }
  */
  /* [3] Customized marker->cell viewer: Marker data is projected onto the velocity mesh. User defines specific fields */
  /*
  {
  const int                    nf = 3;
  const MaterialPointVariable  mp_prop_list[] = { MPV_viscosity, MPV_density, MPV_plastic_strain };

  ierr = pTatin3d_ModelOutput_MarkerCellFields(c,nf,mp_prop_list,prefix);CHKERRQ(ierr);
  }
  */

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Destroy (free) the contents of any objects created within your model context (ModelTemplateCtx).
  - Destroy (free) the model context (ModelTemplateCtx).
 
  Optional method.
*/
static PetscErrorCode ModelDestroy(pTatinCtx c,void *ctx)
{
  ModelTemplateCtx *data;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"[[%s]]\n", PETSC_FUNCTION_NAME);

  data = (ModelTemplateCtx*)ctx;

  /* Free contents of structure */

  /* Free structure */
  ierr = PetscFree(data);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
  Purpose:
  - Create (allocate memory) your model context (ModelTemplateCtx).
  - Initialize any parameters in your model context (ModelTemplateCtx).
  - Define the textual name of your model using pTatinModelSetName(). The model can be selected
    at runtime using the option -ptatin_model MODEL_NAME.
  - Specify all the functions defined within the model object (pTatinModel).
  - Register the model using pTatinModelRegister().
*/
PetscErrorCode pTatinModelRegister_Template(void)
{
  ModelTemplateCtx *data;
  pTatinModel      m;
  PetscErrorCode   ierr;

  PetscFunctionBegin;

  /* Allocate memory for the data structure for this model */
  ierr = PetscCalloc1(1,&data);CHKERRQ(ierr);

  /* set initial values for model parameters */
  data->param1 = 0.0;
  data->param2 = 0;

  /* register user model */
  ierr = pTatinModelCreate(&m);CHKERRQ(ierr);

  /* Set name, model select via -ptatin_model NAME */
  ierr = pTatinModelSetName(m,"template");CHKERRQ(ierr);

  /* Set model data */
  ierr = pTatinModelSetUserData(m,data);CHKERRQ(ierr);

  /* Set function pointers */
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_INIT,                  (void (*)(void))ModelInitialize);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_INIT_MESH_GEOM,  (void (*)(void))ModelApplyInitialMeshGeometry);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_INIT_MAT_GEOM,   (void (*)(void))ModelApplyInitialMaterialGeometry);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_INIT_STOKES_VARIABLE_MARKERS,   (void (*)(void))ModelApplyInitialStokesVariableMarkers);CHKERRQ(ierr);

  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_INIT_SOLUTION,   (void (*)(void))ModelApplyInitialSolution);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_BC,              (void (*)(void))ModelApplyBoundaryCondition);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_BCMG,            (void (*)(void))ModelApplyBoundaryConditionMG);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_MAT_BC,          (void (*)(void))ModelApplyMaterialBoundaryCondition);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_APPLY_UPDATE_MESH_GEOM,(void (*)(void))ModelApplyUpdateMeshGeometry);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_OUTPUT,                (void (*)(void))ModelOutput);CHKERRQ(ierr);
  ierr = pTatinModelSetFunctionPointer(m,PTATIN_MODEL_DESTROY,               (void (*)(void))ModelDestroy);CHKERRQ(ierr);

  /* Insert model into list */
  ierr = pTatinModelRegister(m);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
